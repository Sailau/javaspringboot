package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.SearchKey;
 import kz.aitu.advancedJava.Service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }
    @GetMapping(path="/SearchKey")
    public ResponseEntity<?> getAllRequest(){
        return ResponseEntity.ok(searchKeyService.getAllSearchKey());
    }
    @GetMapping(path="/SearchKey/{id}")
    public ResponseEntity<?> getRequest(@PathVariable long id){
        return ResponseEntity.ok(searchKeyService.getSearchKey(id));}

    @DeleteMapping(path="/SearchKey/{id}")
    public String deleteRequest(@PathVariable int id){
        searchKeyService.deleteSearchKey(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/SearchKey", method= RequestMethod.PUT)
    public ResponseEntity<?> updateRequest(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok(searchKeyService.updateSearchKey(searchKey));
    }
}
