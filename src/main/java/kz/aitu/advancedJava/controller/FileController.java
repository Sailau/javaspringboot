package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.File;
 import kz.aitu.advancedJava.Service.FileService;
 import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }
    @GetMapping(path="/File")
    public ResponseEntity<?> getAllFile(){
        return ResponseEntity.ok(fileService.getAllFile());
    }
    @GetMapping(path="/File/{id}")
    public ResponseEntity<?> getFile(@PathVariable long id){
        return ResponseEntity.ok(fileService.getFile(id));}

    @DeleteMapping(path="/File/{id}")
    public String deleteFile(@PathVariable int id){
        fileService.deleteFile(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/File", method= RequestMethod.PUT)
    public ResponseEntity<?> updateFile(@RequestBody File file){
        return ResponseEntity.ok(fileService.updateFile(file));
    }
}
