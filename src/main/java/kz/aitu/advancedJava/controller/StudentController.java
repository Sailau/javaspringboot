package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.Repository.StudentRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class StudentController {

    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/api/students/{groupId}")
    public ResponseEntity<?> getStudents(@PathVariable Long groupId){

        return ResponseEntity.ok(studentRepository.findAllByGroupId(groupId));

        // return ResponseEntity.ok(studentRepository.getStudents());
 //return ResponseEntity.ok(studentRepository.findAll());
        //List<Student> list = (List<Student>) studentRepository.findAll();
    }
}
