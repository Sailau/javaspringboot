package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.Nomenclature;
 import kz.aitu.advancedJava.Service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }
    @GetMapping(path="/Nomenclature")
    public ResponseEntity<?> getAllNomenclature(){
        return ResponseEntity.ok(nomenclatureService.getAllNomenclature());
    }
    @GetMapping(path="/Nomenclature/{id}")
    public ResponseEntity<?> getNomenclature(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.getNomenclature(id));}

    @DeleteMapping(path="/Nomenclature/{id}")
    public String deleteNomenclature(@PathVariable int id){
        nomenclatureService.deleteNomenclature(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Nomenclature", method= RequestMethod.PUT)
    public ResponseEntity<?> updateNomenclature(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.updateNomenclature(nomenclature));
    }
}
