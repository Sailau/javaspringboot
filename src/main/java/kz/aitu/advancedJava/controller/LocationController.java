package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.Location;
 import kz.aitu.advancedJava.Service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }
    @GetMapping(path="/Location")
    public ResponseEntity<?> getAllLocation(){
        return ResponseEntity.ok(locationService.getAllLocation());
    }
    @GetMapping(path="/Location/{id}")
    public ResponseEntity<?> getLocation(@PathVariable long id){
        return ResponseEntity.ok(locationService.getLocation(id));}

    @DeleteMapping(path="/Location/{id}")
    public String deleteLocation(@PathVariable int id){
        locationService.deleteLocation(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Location", method= RequestMethod.PUT)
    public ResponseEntity<?> updateLocation(@RequestBody Location location){
        return ResponseEntity.ok(locationService.updateLocation(location));
    }
}
