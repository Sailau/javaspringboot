package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.Catalog;
import kz.aitu.advancedJava.Service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
    @GetMapping(path="/Catalog")
    public ResponseEntity<?> getAllCatalog(){
        return ResponseEntity.ok(catalogService.getAllCatalog());
    }
    @GetMapping(path="/Catalog/{id}")
    public ResponseEntity<?> getCatalog(@PathVariable long id){
        return ResponseEntity.ok(catalogService.getCatalog(id));}

    @DeleteMapping(path="/Catalog/{id}")
    public String deleteCatalog(@PathVariable int id){
        catalogService.deleteCatalog(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Catalog", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCatalog(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.updateCatalog(catalog));
    }
}
