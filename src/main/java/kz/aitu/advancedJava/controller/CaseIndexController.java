package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.CaseIndex;
import kz.aitu.advancedJava.Service.CaseIndexService;
 import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CaseIndexController {
    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }
    @GetMapping(path="/CaseIndex")
    public ResponseEntity<?> getAllCaseIndex(){
        return ResponseEntity.ok(caseIndexService.getAllCaseIndex());
    }
    @GetMapping(path="/CaseIndex/{id}")
    public ResponseEntity<?> getCaseIndex(@PathVariable long id){
        return ResponseEntity.ok(caseIndexService.getCaseIndex(id));}

    @DeleteMapping(path="/CaseIndex/{id}")
    public String deleteCaseIndex(@PathVariable int id){
        caseIndexService.deleteCaseIndex(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/CaseIndex", method= RequestMethod.PUT)
    public ResponseEntity<?> updateCaseIndex(@RequestBody CaseIndex a){
        return ResponseEntity.ok(caseIndexService.updateCaseIndex(a));
    }

}
