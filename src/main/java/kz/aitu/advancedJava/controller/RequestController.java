package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.Request;
 import kz.aitu.advancedJava.Service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }
    @GetMapping(path="/Request")
    public ResponseEntity<?> getAllRequest(){
        return ResponseEntity.ok(requestService.getAllRequest());
    }
    @GetMapping(path="/Request/{id}")
    public ResponseEntity<?> getRequest(@PathVariable long id){
        return ResponseEntity.ok(requestService.getRequest(id));}

    @DeleteMapping(path="/Request/{id}")
    public String deleteRequest(@PathVariable int id){
        requestService.deleteRequest(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Request", method= RequestMethod.PUT)
    public ResponseEntity<?> updateRequest(@RequestBody Request request){
        return ResponseEntity.ok(requestService.updateRequest(request));
    }
}
