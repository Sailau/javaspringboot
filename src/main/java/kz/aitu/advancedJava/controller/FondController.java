package kz.aitu.advancedJava.controller;

 import kz.aitu.advancedJava.entity.Fond;
 import kz.aitu.advancedJava.Service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }
    @GetMapping(path="/Fond")
    public ResponseEntity<?> getAllFond(){
        return ResponseEntity.ok(fondService.getAllFond());
    }
    @GetMapping(path="/Fond/{id}")
    public ResponseEntity<?> getFond(@PathVariable long id){
        return ResponseEntity.ok(fondService.getFond(id));}

    @DeleteMapping(path="/Fond/{id}")
    public String deleteFond(@PathVariable int id){
        fondService.deleteFond(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Fond", method= RequestMethod.PUT)
    public ResponseEntity<?> updateFond(@RequestBody Fond fond){
        return ResponseEntity.ok(fondService.updateFond(fond));
    }
}
