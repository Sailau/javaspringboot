package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.Notification;
 import kz.aitu.advancedJava.Service.NotificationService;
 import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    @GetMapping(path="/Notification")
    public ResponseEntity<?> getAllNotification(){
        return ResponseEntity.ok(notificationService.getAllNotification());
    }
    @GetMapping(path="/Notification/{id}")
    public ResponseEntity<?> getNotification(@PathVariable long id){
        return ResponseEntity.ok(notificationService.getNotification(id));}

    @DeleteMapping(path="/Notification/{id}")
    public String deleteNotification(@PathVariable int id){
        notificationService.deleteNotification(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Notification", method= RequestMethod.PUT)
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.updateNotification(notification));
    }
}
