package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.entity.Authorization;
import kz.aitu.advancedJava.Service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
public class AuthorizationController {
    private final AuthorizationService authorizationService;
    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }
    @GetMapping(path="/Authorization")
    public ResponseEntity<?> getAllAuthorization(){
        return ResponseEntity.ok(authorizationService.getAllAuthorization());
    }
    @GetMapping(path="/Authorization/{id}")
    public ResponseEntity<?> getAuthorization(@PathVariable long id){
        return ResponseEntity.ok(authorizationService.getAuthorization(id));}

    @DeleteMapping(path="/Authorization/{id}")
    public String deleteAuthorization(@PathVariable int id){
        authorizationService.deleteAuthorization(id);
        return "Was deleted.";
    }
    @RequestMapping(value="/Authorization", method= RequestMethod.PUT)
    public ResponseEntity<?> updateAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok(authorizationService.updateAuthorization(authorization));
    }
}
