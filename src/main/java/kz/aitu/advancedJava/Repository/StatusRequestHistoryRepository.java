package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.StatusRequestHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRequestHistoryRepository extends CrudRepository<StatusRequestHistory, Long> {

}