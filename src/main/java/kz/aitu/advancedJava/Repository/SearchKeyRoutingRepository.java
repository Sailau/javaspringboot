package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {

}
