package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}