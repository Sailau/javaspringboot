package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Student;
 import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student,Long> {
    @Query(value="select * from students where id=2", nativeQuery = true)
    Student getStudents();

    List<Student> findAllByGroupId(long group_id);
}
