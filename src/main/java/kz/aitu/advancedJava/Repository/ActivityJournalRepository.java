package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {

}
