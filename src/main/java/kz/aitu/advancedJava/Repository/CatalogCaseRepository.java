package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {

}