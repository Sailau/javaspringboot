package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {

}
