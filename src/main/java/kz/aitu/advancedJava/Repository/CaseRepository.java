package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends CrudRepository<Case, Long> {

}