package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {

}
