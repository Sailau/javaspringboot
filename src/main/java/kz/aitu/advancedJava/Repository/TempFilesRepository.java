package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.TempFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {

}
