package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {

}

