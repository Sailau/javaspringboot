package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {

}