package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {

}