package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {

}