package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {

}
