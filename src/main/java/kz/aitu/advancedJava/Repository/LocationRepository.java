package kz.aitu.advancedJava.Repository;

 import kz.aitu.advancedJava.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {

}