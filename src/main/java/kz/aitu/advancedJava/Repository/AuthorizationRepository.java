package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends CrudRepository<Authorization, Long> {

}
