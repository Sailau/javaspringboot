package kz.aitu.advancedJava.Repository;

import kz.aitu.advancedJava.entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {

}
