package kz.aitu.advancedJava.Service;

import kz.aitu.advancedJava.entity.CaseIndex;
import kz.aitu.advancedJava.Repository.CaseIndexRepository;
 import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class CaseIndexService {
    CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }
    public List<CaseIndex> getAllCaseIndex(){
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }
    public Optional<CaseIndex> getCaseIndex(long id) {
        return caseIndexRepository.findById(id);
    }

    public void deleteCaseIndex(long id){
        caseIndexRepository.deleteById(id);
    }
    public CaseIndex updateCaseIndex(@RequestBody CaseIndex a){
        return  caseIndexRepository.save(a);
    }
}
