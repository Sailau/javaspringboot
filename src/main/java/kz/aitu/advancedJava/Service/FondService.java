package kz.aitu.advancedJava.Service;

 import kz.aitu.advancedJava.entity.Fond;
 import kz.aitu.advancedJava.Repository.FondRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FondService {
    FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAllFond(){
        return (List<Fond>) fondRepository.findAll();
    }
    public Optional<Fond> getFond(long id) {
        return fondRepository.findById(id);
    }

    public void deleteFond(long id){
        fondRepository.deleteById(id);
    }
    public Fond updateFond(@RequestBody Fond fond){
        return  fondRepository.save(fond);
    }
}
