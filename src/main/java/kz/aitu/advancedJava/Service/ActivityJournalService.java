package kz.aitu.advancedJava.Service;

import kz.aitu.advancedJava.entity.ActivityJournal;
import kz.aitu.advancedJava.Repository.ActivityJournalRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class ActivityJournalService {
    ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAllActivityJournal(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }
    public Optional<ActivityJournal> getActivityJournal(long id) {
        return activityJournalRepository.findById(id);
    }

    public void deleteActivityJournal(long id){
        activityJournalRepository.deleteById(id);
    }
    public ActivityJournal updateActivityJournal(@RequestBody ActivityJournal activityJournal){
        return  activityJournalRepository.save(activityJournal);
    }
}
