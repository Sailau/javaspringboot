package kz.aitu.advancedJava.Service;

 import kz.aitu.advancedJava.entity.SearchKey;
 import kz.aitu.advancedJava.Repository.SearchKeyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class SearchKeyService {
    SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public List<SearchKey> getAllSearchKey(){
        return (List<SearchKey>) searchKeyRepository.findAll();
    }
    public Optional<SearchKey> getSearchKey(long id) {
        return searchKeyRepository.findById(id);
    }
    public void deleteSearchKey(long id){
        searchKeyRepository.deleteById(id);
    }
    public SearchKey updateSearchKey(@RequestBody SearchKey searchKey){
        return  searchKeyRepository.save(searchKey);
    }
}
