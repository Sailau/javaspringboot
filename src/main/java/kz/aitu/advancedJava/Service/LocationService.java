package kz.aitu.advancedJava.Service;

 import kz.aitu.advancedJava.entity.Location;
 import kz.aitu.advancedJava.Repository.LocationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class LocationService {
    LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAllLocation(){
        return (List<Location>) locationRepository.findAll();
    }
    public Optional<Location> getLocation(long id) {
        return locationRepository.findById(id);
    }

    public void deleteLocation(long id){
        locationRepository.deleteById(id);
    }
    public Location updateLocation(@RequestBody Location location){
        return  locationRepository.save(location);
    }
}
