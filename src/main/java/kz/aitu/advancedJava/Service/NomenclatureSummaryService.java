package kz.aitu.advancedJava.Service;

 import kz.aitu.advancedJava.entity.NomenclatureSummary;
 import kz.aitu.advancedJava.Repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class NomenclatureSummaryService {
    NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public List<NomenclatureSummary> getAllNomenclatureSummary(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }
    public Optional<NomenclatureSummary> getNomenclatureSummary(long id) {
        return nomenclatureSummaryRepository.findById(id);
    }

    public void deleteNomenclatureSummary(long id){
        nomenclatureSummaryRepository.deleteById(id);
    }
    public NomenclatureSummary updateNomenclatureSummary(@RequestBody NomenclatureSummary n){
        return  nomenclatureSummaryRepository.save(n);
    }


}
