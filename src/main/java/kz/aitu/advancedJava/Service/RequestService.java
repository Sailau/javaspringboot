package kz.aitu.advancedJava.Service;

import kz.aitu.advancedJava.entity.Request;
 import kz.aitu.advancedJava.Repository.RequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class RequestService {
    RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAllRequest(){
        return (List<Request>) requestRepository.findAll();
    }
    public Optional<Request> getRequest(long id) {
        return requestRepository.findById(id);
    }
    public void deleteRequest(long id){
        requestRepository.deleteById(id);
    }
    public Request updateRequest(@RequestBody Request request){
        return  requestRepository.save(request);
    }
}
