package kz.aitu.advancedJava.Service;

 import kz.aitu.advancedJava.entity.Nomenclature;
 import kz.aitu.advancedJava.Repository.NomenclatureRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
@Service
public class NomenclatureService {
    NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAllNomenclature(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }
    public Optional<Nomenclature> getNomenclature(long id) {
        return nomenclatureRepository.findById(id);
    }

    public void deleteNomenclature(long id){
        nomenclatureRepository.deleteById(id);
    }
    public Nomenclature updateNomenclature(@RequestBody Nomenclature nomenclature){
        return  nomenclatureRepository.save(nomenclature);
    }

}
