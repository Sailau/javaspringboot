package kz.aitu.advancedJava.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class Group {
    private long id;
    private String name;

}
