package kz.aitu.advancedJava.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name="students")
public class Student {
@Id
    private long id;
    private String name;
    private String phone;
    private long groupId;


}
