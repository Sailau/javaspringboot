DROP table if exists actdestroy;
create table actdestroy
(
    id bigint not null
        constraint actdestroy_pkey
            primary key,
    no_act varchar(128),
    base varchar(256),
    company_unit_id bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table actdestroy owner to postgres;
DROP table if exists activityjournal;

create table activityjournal
(
    id bigint not null
        constraint activityjournal_pkey
            primary key,
    event_type varchar(128),
    object_type varchar(255),
    object_id bigint,
    created_timestamp bigint,
    created_by bigint,
    message_level varchar(128),
    message varchar(255)
);

alter table activityjournal owner to postgres;
DROP table if exists autorization;

create table autorization
(
    id bigint not null
        constraint autorization_pkey
            primary key,
    login varchar(255),
    email varchar(255),
    password varchar(128),
    role varchar(255),
    forgot_password_key varchar(128),
    forgot_password_key_time bigint,
    company_unit_id bigint
);

alter table autorization owner to postgres;

DROP table if exists catalog;

create table catalog
(
    id bigint not null
        constraint catalog_pkey
            primary key,
    name_ru varchar(128),
    name_kz varchar(128),
    name_en varchar(128),
    parent_id bigint,
    company_unit_id bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table catalog owner to postgres;
DROP table if exists companyunit;

create table companyunit
(
    id bigint not null
        constraint companyunit_pkey
            primary key,
    name_ru varchar(128),
    name_kz varchar(128),
    name_en varchar(128),
    parent_id bigint,
    year integer,
    company_id integer,
    code_index varchar(16),
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table companyunit owner to postgres;
DROP table if exists company;

create table  company
(
    id bigint not null
        constraint company_pkey
            primary key,
    name_ru varchar(128),
    name_kz varchar(128),
    name_en varchar(128),
    bin varchar(32),
    parent_id bigint,
    fond_id bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table company owner to postgres;
DROP table if exists fond;

create table fond
(
    id bigint not null
        constraint fond_pkey
            primary key,
    fond_number varchar(128),
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table fond owner to postgres;
DROP TABLE IF EXISTS casee;

create table casee
(
    id bigint not null
        constraint case_pkey
            primary key,
    no_case varchar(128),
    no_tome varchar(128),
    case_ru varchar(128),
    case_kz varchar(128),
    case_en varchar(128),
    start_data bigint,
    finish_data bigint,
    no_pages bigint,
    is_eds boolean,
    eds text,
    sending_sign_naf boolean,
    deleting_sign boolean,
    restricted_flag boolean,
    hash varchar(128),
    version integer,
    id_version varchar(128),
    is_active boolean,
    note varchar(255),
    location_id bigint,
    index_case_id bigint,
    sign_id bigint,
    act_destroy_id bigint,
    structural_separation_id bigint,
    address_case_blockchain varchar(128),
    date_adding_blockchain bigint,
    date_crating bigint,
    created_bywho bigint,
    date_change bigint,
    changed_bywho bigint
);

alter table casee owner to postgres;

create table if not exists filerouting
(
    id bigint not null,
    file_id bigint,
    table_name varchar(128),
    table_id bigint,
    type varchar(128)
);

alter table filerouting owner to postgres;
DROP TABLE IF EXISTS location;

create table  location
(
    id bigint not null
        constraint location_pkey
            primary key,
    row varchar(64),
    line varchar(64),
    columnn varchar(64),
    box varchar(64),
    company_unit_id bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table location owner to postgres;
DROP TABLE IF EXISTS record;

create table record
(
    id bigint not null
        constraint record_pkey
            primary key,
    number varchar(128),
    type varchar(128),
    company_unit_id bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table record owner to postgres;
DROP TABLE IF EXISTS caseindex;

create table caseindex
(
    id bigint not null
        constraint caseindex_pkey
            primary key,
    case_index varchar(128),
    title_ru varchar(128),
    title_kz varchar(128),
    title_en varchar(128),
    storage_type integer,
    storage_year integer,
    note varchar(128),
    company_unit_id bigint
    ,
    nomenclature_id bigint
        ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table caseindex owner to postgres;
DROP TABLE IF EXISTS nomenclature;

create table  nomenclature
(
    id bigint not null
        constraint nomenclature_pkey
            primary key,
    nomenclature_no varchar(128),
    year integer,
    nomenclature_summary_id bigint
       ,
    company_unit_id bigint
        ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table nomenclature owner to postgres;
DROP TABLE IF EXISTS nomenclaturesummary;

create table nomenclaturesummary
(
    id bigint not null
        constraint nomenclaturesummary_pkey
            primary key,
    number varchar(128),
    year integer,
    company_unit_id bigint
         ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table nomenclaturesummary owner to postgres;
DROP TABLE IF EXISTS catalogcase;

create table catalogcase
(
    id bigint not null
        constraint catalogcase_pkey
            primary key,
    case_id bigint
         ,
    catalog_id bigint
        ,
    company_unit_id bigint
       ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table catalogcase owner to postgres;
DROP TABLE IF EXISTS file;

create table  file
(
    id bigint not null
        constraint file_pkey
            primary key,
    name varchar(128),
    type varchar(128),
    size bigint,
    page_count integer,
    hash varchar(128),
    is_deleted boolean,
    file_binary_id bigint
       ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table file owner to postgres;
DROP TABLE IF EXISTS tempfiles;

create table tempfiles
(
    id bigint not null
        constraint tempfiles_pkey
            primary key,
    file_binary text,
    file_binary_byte bytea
);

alter table tempfiles owner to postgres;
DROP TABLE IF EXISTS notification;

create table notification
(
    id bigint not null
        constraint notification_pkey
            primary key,
    object_type varchar(128),
    object_id bigint,
    company_unit_id bigint,
    user_id bigint,
    created_timestamp bigint,
    viewed_timestamp bigint,
    is_viewed boolean,
    title varchar(255),
    text varchar(255),
    company_id bigint
);

alter table notification owner to postgres;
DROP TABLE IF EXISTS request;

create table request
(
    id bigint not null
        constraint request_pkey
            primary key,
    request_user_id bigint,
    response_user_id bigint,
    case_id bigint,
    case_index_id bigint
       ,
    created_type varchar(64),
    comment varchar(255),
    status varchar(64),
    timestamp bigint,
    sharestart bigint,
    sharefinish bigint,
    favorite boolean,
    update_timestamp bigint,
    update_by bigint,
    declinenote varchar(255),
    company_unit_id bigint
       ,
    from_request_id bigint
);

alter table request owner to postgres;

DROP TABLE IF EXISTS searchkey;

create table  searchkey
(
    id bigint not null
        constraint searchkey_pkey
            primary key,
    name varchar(128),
    company_unit_id bigint
       ,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table searchkey owner to postgres;
DROP TABLE IF EXISTS searchkeyrouting;

create table searchkeyrouting
(
    id bigint not null
        constraint searchkeyrouting_pkey
            primary key,
    search_key_id bigint
       ,
    table_name varchar(128),
    table_id bigint
        ,
    type varchar(128)
);

alter table searchkeyrouting owner to postgres;
DROP TABLE IF EXISTS share;

create table share
(
    id bigint not null
        constraint share_pkey
            primary key,
    request_id bigint
        ,
    note varchar(255),
    sender_id bigint,
    receiver_id bigint,
    share_timestamp bigint
);

alter table share owner to postgres;
DROP TABLE IF EXISTS statusrequesthistory;

create table statusrequesthistory
(
    id bigint not null
        constraint statusrequesthistory_pkey
            primary key,
    request_id bigint
       ,
    status varchar(64),
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table statusrequesthistory owner to postgres;
DROP TABLE IF EXISTS userr;

create table userr
(
    id bigint not null
        constraint user_pkey
            primary key,
    auth_id bigint
         ,
    name varchar(128),
    surname varchar(128),
    secondname varchar(128),
    status varchar(128),
    company_unit_id bigint
        ,
    password varchar(128),
    lastlogin_time bigint,
    iin varchar(32),
    is_active boolean,
    is_activated boolean,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint
);

alter table userr owner to postgres;


